# WEB-T Translation Hub
European Multilingual Web (EMW) project is a response to the LOT1 “Solutions Supporting the Use of Automated Translations on Websites” of the European Commission tender “Language Technology Solutions – CNECT/LUX/2022/OP/0030”.

---
## Architecture
The toolkit consists of several microservices, each component is deployable as docker container.

---
### Components:
**Frontend** (frontend) – Application responsible for all GUI and its operations. It is using Angular, that means this is Single Page Application

**Translation Hub API** (websiteTranslationService) – service for translation management

**Configuration service** (configurationService) - service for configurations

**MT API Integrator** (mtApiIntegrator) – service for MT services

**eTranslation Provider** (eTranslationProxy ) – service for eTranslation implementation

**Crawler Agent** (crawlerAgent) – service for Website pretranslation

**Redis** - DB for eTranslation

**MySQL** - DB for APIs

---
## Minimal system requirements
- At least a 2-core x86/x64 CPU (Central Processor Unit)
- 5 GB of RAM (Random Access Memory) or more
- 30 GB of storage or more (for DB)
- Internet access

## Supported software versions
- Kubernetes cluster version 1.25+
- Helm 3.5+
- MySQL server 8.0
---
## Kubernetes cluster

You will need `kubectl` ([instructions](https://kubernetes.io/docs/tasks/tools/#kubectl))
and `Helm` ([instructions](https://helm.sh/docs/intro/install/)) installed.

### Running WEB-T Translation Hub locally in a dev environment

For testing and development purposes, an easy option to get Kubernetes running
is to install [Rancher Desktop](https://rancherdesktop.io/). Once you have it
installed, you will also need to setup an ingress controller. Rancher uses
*Traefik* as the default one, so disable it first by unchecking `Enable Traefik`
from the `Kubernetes Settings` page. Then deploy the NGINX ingress controller:
```
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```
---
## MySQL Databases

### Setup WEB-T databases on MySQL 8.0 server
> Please note: use separate on-premise or cloud hosted MySQL server or run MySQL server in Kubernetes. MySQL server must be reachable from Kubernetes [Pods](https://kubernetes.io/docs/concepts/workloads/pods/)

### Prepare empty WEB-T databases (DB) and users for databases:
* adjust DB, DB user names and DB user passwords as needed, must match with the database names in your Helm chart `values.yaml` file.

```sql
-- For configurationService component
CREATE DATABASE IF NOT EXISTS `webt_configuration` CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER IF NOT EXISTS 'webt_configuration_user'@'%' IDENTIFIED WITH mysql_native_password BY 'webt_configuration_user_pass';
GRANT ALL PRIVILEGES ON `webt_configuration`.* TO 'webt_configuration_user'@'%';

-- For websiteTranslationService component
CREATE DATABASE IF NOT EXISTS `webt_website_translation` CHARACTER SET utf8 COLLATE utf8_unicode_ci;
CREATE USER IF NOT EXISTS 'webt_website_translation_user'@'%' IDENTIFIED WITH mysql_native_password BY 'webt_website_translation_user_pass';
GRANT ALL PRIVILEGES ON `webt_website_translation`.* TO 'webt_website_translation_user'@'%';
```
---
## Build WEB-T container images and push containers to your container registry

| Component                 | Source code repository | Corresponding [Helm chart](https://code.europa.eu/web-t/web-t-translation-hub-containerable/deployment/-/tree/main/translation-hub/templates) yaml |
| ------------------------- | --------------------------------------------------------------------------------------------------------------- | --------------------------------------------|
| configurationService      | [Configuration service](https://code.europa.eu/web-t/web-t-translation-hub-containerable/configuration-service) | `configuration-service.yaml`                |
| eTranslationProxy         | [eTranslation provider](https://code.europa.eu/web-t/web-t-translation-hub-containerable/etranslation-provider) | `translation-proxy-e-translation.yaml`      |
| frontend                  | [Frontend](https://code.europa.eu/web-t/web-t-translation-hub-containerable/frontend)                           | `frontend.yaml`                             |
| mtApiIntegrator           | [MT API Integrator](https://code.europa.eu/web-t/web-t-translation-hub-containerable/mt-api-integrator)         | `mt-api-integrator.yaml`                    |
| websiteTranslationService | [API](https://code.europa.eu/web-t/web-t-translation-hub-containerable/api)                                     | `website-translation-service.yaml`          |
| crawlerAgent              | [Crawler Agent](https://code.europa.eu/web-t/web-t-translation-hub-containerable/frontend/crawler-agent)        | `crawler-agent.yaml`                        |

- Read about building docker container images [here](https://docs.docker.com/engine/reference/commandline/build/)

---
## Installing the chart

### Using the chart from GitHub repo

1. Clone this repository

   ```shell
   git clone https://code.europa.eu/web-t/web-t-translation-hub-containerable/deployment
   cd deployment/translation-hub
   ```

2. Make sure to update `values.yaml` with corresponding values or create new values file.

   The following table lists the configurable parameters of the Translation Hub chart. The current default values can be found in `values.yaml` file.

   | Parameters                                                 | Description                                                                                        |
   | ---------------------------------------------------------- | ---------------------------------------------------------------------------------------------------|
   | basicAuth.enabled                                          | Enable use of basic authentication for the frontend access - set "true" or "false"                 |
   | basicAuth.credentials                                      | [Optional] Set credentials for basic authentication                                                |
   | configurationService.dbConnectionString                    | Configuration Service Database connection string                                                   |
   | configurationService.image.name                            | Configuration Service Image name                                                                   |
   | configurationService.image.pullSecret                      | Configuration Service Image pull secret                                                            |
   | configurationService.image.repository                      | Configuration Service Image repository                                                             |
   | configurationService.image.tag                             | Configuration Service Image tag                                                                    |
   | eTranslationProxy.image.name                               | ETranslation Proxy Image name                                                                      |
   | eTranslationProxy.image.pullSecret                         | ETranslation Proxy Image pull secret                                                               |
   | eTranslationProxy.image.repository                         | ETranslation Proxy Image repository                                                                |
   | eTranslationProxy.image.tag                                | ETranslation Proxy Image tag                                                                       |
   | frontend.image.name                                        | Frontend Image name                                                                                |
   | frontend.image.pullSecret                                  | Frontend Image pull secret                                                                         |
   | frontend.image.repository                                  | Frontend Image repository                                                                          |
   | frontend.image.tag                                         | Frontend Image tag                                                                                 |
   | global.domain                                              | Translation Hub domain name                                                                        |
   | global.eTranslation.email                                  | eTranslation translation recipient email - for testing only, leave empty after testing             |
   | global.eTranslation.timeout                                | eTranslation timeout                                                                               |
   | global.translationImport.maxTranslationImportFileSizeBytes | Max translation import file size in bytes                                                          |
   | ingress.tlsSecret                                          | Ingress TLS secret name                                                                            |
   | mtApiIntegrator.image.name                                 | MT API Integrator Image name                                                                       |
   | mtApiIntegrator.image.pullSecret                           | MT API Integrator Image pull secret                                                                |
   | mtApiIntegrator.image.repository                           | MT API Integrator Image repository                                                                 |
   | mtApiIntegrator.image.tag                                  | MT API Integrator Image tag                                                                        |
   | websiteTranslationService.dbConnectionString               | Website translation service Database connection string                                             |
   | websiteTranslationService.image.name                       | Website translation service Image name                                                             |
   | websiteTranslationService.image.pullSecret                 | Website translation service Image pull secret                                                      |
   | websiteTranslationService.image.repository                 | Website translation service Image repository                                                       |
   | websiteTranslationService.image.tag                        | Website translation service Image tag                                                              |
   | crawlerAgent.image.name                                    | Crawler Agent Image name                                                                           |
   | crawlerAgent.image.pullSecret                              | Crawler Agent Image pull secret                                                                    |
   | crawlerAgent.image.repository                              | Crawler Agent Image repository                                                                     |
   | crawlerAgent.image.tag                                     | Crawler Agent Image tag                                                                            |
   | crawlerAgent.config.serverTimeout                          | server timeout in seconds                                                                          |
   | crawlerAgent.config.pretranslationDelay                    | Delay pretranslation with next language (seconds)                                                  |
   | crawlerAgent.config.pretranslationTimeout                  | Pretranslation timeout in seconds                                                                  |
   | crawlerAgent.config.minConcurrency                         | Minimum concurrency (parallelism) for the crawl                                                    |
   | crawlerAgent.config.maxConcurrency                         | Maximum concurrency for the crawl (adjust CPU and RAM resource limits accordingly)                 |
   | crawlerAgent.config.maxRequestsPerMinute                   | The maximum number of requests per minute the crawler should run                                   |
   | crawlerAgent.config.maxRequestRetries                      | Indicates how many times the request is retried if requestHandler fails                            |
   | crawlerAgent.config.maxRequestsPerCrawl                    | Maximum number of pages that the crawler will open. The crawl will stop when this limit is reached |

   **Optional**: Configuration for Restricting Access to translation hub website (Basic Authentication):

      Credential preparation for Basic Authentication configuration: \
      If basicAuth.enabled is set to `true` then user credentials must be configured in `values.yaml` file.

      Example on how to prepare Basic Auth configuration for `web-t-user` user with password `web-t-pwd`:
     
      ```shell
     #  In Linux terminal execute command:
      htpasswd -nb web-t-user web-t-pwd
      ```
      Command output example: `web-t-user:$apr1$j2PQtFMu$7uEKTHLBdxwAbYKvgdfmh/`


     
      ```shell
      # Next, copy part of previous output - the part after username and colon symbol (e.g. part after "web-t-user:") and paste that to second command to get the base64-encoded password hash to use as a password:
      printf '$apr1$j2PQtFMu$7uEKTHLBdxwAbYKvgdfmh/' | base64
      ```
      Command output example, use this for password configuration: `JGFwcjEkajJQUXRGTXUkN3VFS1RITEJkeHdBYllLdmdkZm1oLw==`

      Update configuration in `values.yaml` file:
      ```yaml
      basicAuth:
        enabled: true
        credentials:
          - username: web-t-user
            password: JGFwcjEkajJQUXRGTXUkN3VFS1RITEJkeHdBYllLdmdkZm1oLw==
         # you can add more users if necessary by adding these lines with user prepared user credentials:
          - username: user2      # to add another user
            password: *pwd2*  # tanother users pwd
      ```

3. Install the chart with the release name `translation-hub` in `web-t` namespace.

    ```shell
    helm install --create-namespace -n web-t translation-hub . -f values.yaml
    ```

> Please note: eTranslation is an online machine translation service provided by the European Commission (EC).
> eTranslation web service is asynchronous. This means that the client sends a translation request and is notified later when the text snippet is translated. Make sure that translation hub API domain is correctly exposed to the internet and can receive traffic from eTranslation servers.
---
## Uninstalling the chart

To uninstall/delete the `translation-hub` deployment from `web-t` namespace, run:

```shell
helm delete translation-hub --namespace web-t
```

Delete `web-t` namespace
```shell
kubectl delete namespace web-t
```
